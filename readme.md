# 🌱
## A CSS Framework

🌱 is a simple, responsive CSS grid system and basic framework, designed to be overwritten.

You can include in your project by running using npm.

```
npm install git+ssh://git@bitbucket.org/lewisdorigo/sprout-css.git --save-dev
```

If you’re using `gulp` and `gulp-ruby-sass` to compile your SCSS/SASS into CSS, you can include 🌱 easily.

`gulpfile.js`:
```
'use strict';

const gulp        = require('gulp');
const sass        = require('gulp-ruby-sass');

const sprout      = require('sprout-css').includePaths;

gulp.task('sass', function() {
    return sass(['./styles.scss', './styles.sass'],{
            sourcemap: true,
            trace: true,
            style: 'compressed',
            loadPath: [].concat(sprout),
        })
        .on('error', sass.logError)
        .pipe(gulp.dest('./css/'));
});
```

`styles.scss`:
```
/**
 * Overrides for 🌱 variables
 */
@import 'vars';


/**
 * Include 🌱
 */
@import '🌱';


/**
 * Include your project’s styles
 */
@import 'project.css/styles';
```